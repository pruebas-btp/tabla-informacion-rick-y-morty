sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, JSONModel) {
        "use strict";

        return Controller.extend("rickandmorty.controller.App", {
            obtenerDatos: function(){
                $.ajax({
                    url: "https://rickandmortyapi.com/api/character",
                    type: "GET",
                    async: true,
                    cache: false,
                    context: this,
                    contentType: "application/json",
                    beforeSend: function () {
                        sap.ui.core.BusyIndicator.show(0);
                    },
                    success: function (oResponse) {
                        let oModel = new JSONModel(oResponse.results);
                        this.getView().setModel(oModel, "oFilas");
                    },
                    error: function (oError) {
                        console.log(oError);
                    },
                    complete: function () {
                        sap.ui.core.BusyIndicator.hide();
                    },
                }); 
            },
            onInit: function(){
                this.obtenerDatos();
            }
        });
    });

   