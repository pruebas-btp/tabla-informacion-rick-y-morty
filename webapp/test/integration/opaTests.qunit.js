/* global QUnit */

sap.ui.require(["rickandmorty/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
